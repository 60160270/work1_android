fun main(){
    infix fun Int.times(str:String) = str.repeat(this)
    println(2 times "Bye ")

    val pair = "one" to "two"
    println(pair)

    infix fun String.onto(other: String) = Pair(this, other)
    val myPair = "McLaren" onto "Lucas"
    println(myPair)
}